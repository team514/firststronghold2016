package frc.robot;

import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import frc.robot.commands.AutoCourtyard;
import frc.robot.commands.CalibrateGyro;
import frc.robot.commands.DriveArcade;
import frc.robot.commands.DriveArcadeDualshock;
import frc.robot.commands.DriveRacing;
import frc.robot.commands.DriveTank;
import frc.robot.commands.DriveTankDualshock;
import frc.robot.commands.MagIn;
import frc.robot.commands.MagOut;
import frc.robot.commands.ResetEncoders;
import frc.robot.commands.ResetGyro;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {
	
	//// CREATING BUTTONS
    // One type of button is a joystick button which is any button on a joystick.
    // You create one by telling it which joystick it's on and which button
    // number it is.
    // Joystick stick = new Joystick(port);
    // Button button = new JoystickButton(stick, buttonNumber);
    
    // There are a few additional built in buttons you can use. Additionally,
    // by subclassing Button you can create custom triggers and bind those to
    // commands the same as any other Button.
    
    //// TRIGGERING COMMANDS WITH BUTTONS
    // Once you have a button, it's trivial to bind it to a button in one of
    // three ways:
    
    // Start the command when the button is pressed and let it run the command
    // until it is finished as determined by it's isFinished method.
    // button.whenPressed(new ExampleCommand());
    
    // Run the command while the button is being held down and interrupt it once
    // the button is released.
    // button.whileHeld(new ExampleCommand());
    
    // Start the command when the button is released  and let it run the command
    // until it is finished as determined by it's isFinished method.
    // button.whenReleased(new ExampleCommand());
	
	Joystick leftStick, rightStick, controller, dualshockButtons;
	XboxController dualshockSticks;
	JoystickButton tankDualshock, tankJoystick, arcadeDualshock, arcadeJoystick, racing, magforward, magreverse, resetencoders, autocmd1, calibrategyro, resetgyro;
	
	public OI(){
	//Because Joysticks can be remapped in the Driver Station,
	//RobotMap Interface definitions are not needed.
		leftStick = new Joystick(0);
		rightStick = new Joystick(1);
		controller = new Joystick(2);
		dualshockButtons = new Joystick(3);
		dualshockSticks = new XboxController(3);
		
		tankDualshock = new JoystickButton(dualshockButtons, RobotMap.kDualshockXButtonNum);
		arcadeDualshock = new JoystickButton(dualshockButtons, RobotMap.kDualshockBButtonNum);
		racing = new JoystickButton(dualshockButtons, RobotMap.kDualshockAButtonNum);
		tankJoystick = new JoystickButton(rightStick, RobotMap.tankMode);
		arcadeJoystick = new JoystickButton(rightStick, RobotMap.arcadeMode);
		magforward = new JoystickButton(controller, RobotMap.magIn);
		magreverse = new JoystickButton(controller, RobotMap.magOut);
		resetencoders = new JoystickButton(controller, RobotMap.resetEncoders);
		autocmd1 = new JoystickButton(controller, RobotMap.autoCMD1);
		calibrategyro = new JoystickButton(leftStick, RobotMap.calibrateGyro);
		resetgyro = new JoystickButton(leftStick, RobotMap.resetGyro);
		
		tankJoystick.whenPressed(new DriveTank());
		tankDualshock.whenPressed(new DriveTankDualshock());
		arcadeJoystick.whenPressed(new DriveArcade());
		arcadeDualshock.whenPressed(new DriveArcadeDualshock());
		racing.whenPressed(new DriveRacing());
		magforward.whileHeld(new MagIn());
		magreverse.whileHeld(new MagOut());
		resetencoders.whenPressed(new ResetEncoders());
		autocmd1.whenPressed(new AutoCourtyard());
		calibrategyro.whenPressed(new CalibrateGyro());
		resetgyro.whenPressed(new ResetGyro());
	}
	public double getLeftTriggerY(){
		return dualshockSticks.getTriggerAxis(Hand.kLeft);
	}
	
	public double getRightTriggerY(){
		return dualshockSticks.getTriggerAxis(Hand.kRight);
	}
	public double getLeftStickX(){
		return dualshockSticks.getX(Hand.kLeft);
	}
	
	public double getLeftStickY(){
		return dualshockSticks.getY(Hand.kLeft);
	}
	
	public double getRightStickX(){
		return dualshockSticks.getX(Hand.kRight);
	}
	public double getRightStickY(){
		return dualshockSticks.getY(Hand.kRight);
	}
	public double getLeftJoystickX(){
		return leftStick.getX();
	}
	public double getLeftJoystickY(){
		return leftStick.getY();
	}
	public double getRightJoystickX(){
		return rightStick.getX();
	}
	public double getRightJoystickY(){
		return rightStick.getY();
	}
}

