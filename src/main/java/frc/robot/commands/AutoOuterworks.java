package frc.robot.commands;

import frc.robot.Robot;
import frc.robot.RobotMap;

import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class AutoOuterworks extends Command {

    public AutoOuterworks() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
    	requires(Robot.driveUtil);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    	Robot.driveUtil.resetEncoders();
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    	Robot.driveUtil.driveTank(RobotMap.leftSpeed, RobotMap.rightSpeed);
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
    	boolean done = false;
    	if(Robot.driveUtil.getLeftEncoder()>=RobotMap.oDistance){
    		done = true;
    	}	
        return done;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
