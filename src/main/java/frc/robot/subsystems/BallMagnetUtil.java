package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.RobotMap;
import frc.robot.commands.MagOff;

/**
 *
 */
public class BallMagnetUtil extends Subsystem {
    
    // Put methods for controlling this subsystem
    // here. Call these from Commands.
	
	WPI_TalonSRX magMotor;
	DigitalInput magLimit;
	//to do: decide on what limit switch to use

	public BallMagnetUtil(){
		magMotor = new WPI_TalonSRX(RobotMap.magMotor);
		magLimit = new DigitalInput(RobotMap.magLimit);
	}
	
	public boolean getMagLimit(){
		return magLimit.get();
	}
	
	public void setMotorSpeed(double d){
		magMotor.set(d);
	}
	
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    	setDefaultCommand(new MagOff());
    }
    
    public void updateStatus(){
    	SmartDashboard.putNumber("Ball Motor Voltage: ", magMotor.getBusVoltage());
    	SmartDashboard.putBoolean("Ball Magnet Filled", magLimit.get());
    }
}

