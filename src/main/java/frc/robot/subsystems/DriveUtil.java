
package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import edu.wpi.first.wpilibj.AnalogGyro;
import edu.wpi.first.wpilibj.CounterBase;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.interfaces.Gyro;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.commands.DriveTank;

/**
 *
 */
public class DriveUtil extends Subsystem {
    
    // Put methods for controlling this subsystem
    // here. Call these from Commands.

	WPI_TalonSRX lDriveMaster, lDriveSlave, rDriveMaster, rDriveSlave;
	Encoder leftDriveEncoder, rightDriveEncoder;
	Gyro gyro;
	
	double input, delta, speed;
	
	public DriveUtil(){
		gyro = new AnalogGyro(1);
		lDriveMaster = new WPI_TalonSRX(RobotMap.leftMotorMaster);
		// lDriveMaster.changeControlMode(TalonControlMode.PercentVbus);
		// lDriveMaster.enable();
		
		rDriveMaster = new WPI_TalonSRX(RobotMap.rightMotorMaster);
		// rDriveMaster.changeControlMode(TalonControlMode.PercentVbus);
		// rDriveMaster.enable();
		
		lDriveSlave = new WPI_TalonSRX(RobotMap.leftMotorSlave);
		lDriveSlave.follow(lDriveMaster);
		// lDriveSlave.changeControlMode(TalonControlMode.Follower);
		// lDriveSlave.set(lDriveMaster.getDeviceID());
		// lDriveSlave.enable();
		
		rDriveSlave = new WPI_TalonSRX(RobotMap.rightMotorSlave);
		rDriveSlave.follow(rDriveMaster);
		// rDriveSlave.changeControlMode(TalonControlMode.Follower);
		// rDriveSlave.set(rDriveMaster.getDeviceID());
		// rDriveSlave.enable();
		
		leftDriveEncoder = new Encoder(RobotMap.leftEncoder1, RobotMap.leftEncoder2, true, CounterBase.EncodingType.k4X);
    	rightDriveEncoder = new Encoder(RobotMap.rightEncoder1, RobotMap.rightEncoder2, true, CounterBase.EncodingType.k4X);
    	
    	input = delta = speed = 0.0;

	}
	
	public void driveTank(double leftY, double rightY){
		lDriveMaster.set(squareInput(-leftY));
		rDriveMaster.set(squareInput(rightY));
	}
	
	public void driveArcade(double throttleY, double turnX){
		double left;
        double right;
        left = throttleY - turnX;
        right = throttleY + turnX;
        driveTank(left, right);
	}

	public void driveRacing(double rightStickX, double leftTrigger, double rightTrigger){
		double rightSpeed, leftSpeed;
		rightSpeed = rightTrigger - leftTrigger;
		leftSpeed = rightTrigger - leftTrigger;

		if(rightStickX > 0){
			leftSpeed = rightTrigger - rightStickX;
			rightSpeed = rightTrigger;
		}else{
			if(rightStickX < 0){
				rightSpeed = rightTrigger + rightStickX;
				leftSpeed = rightTrigger;
			}else{
				rightSpeed = rightTrigger;
				leftSpeed = rightTrigger;
			}
		}
		double finalLeftSpeed = leftSpeed - leftTrigger;
		double finalRightSpeed = rightSpeed - leftTrigger;
		
		driveTank(finalLeftSpeed, finalRightSpeed);
	}

	public double squareInput(double d) {
		boolean negative = d < 0;
		d = Math.pow(d,2);
		if(negative){
			d *= -1;
		}
		return d;
	}
	
	public double getLeftEncoder(){
    	return leftDriveEncoder.getRaw();
    }
    
    public double getRightEncoder(){
    	return rightDriveEncoder.getRaw();
    }
    
    public void resetEncoders(){
    	leftDriveEncoder.reset();
    	rightDriveEncoder.reset();
    }
    
    public void resetGyro(){
    	gyro.reset();
    }
    
    public void calibrateGyro(){
    	gyro.calibrate();
    }
    
    public double coerce2Range(double input){
        double inputMin, inputMax, inputCenter;
        double outputMin, outputMax, outputCenter;
        double scale, result;
        //double output;
        
        inputMin = RobotMap.C2R_inputMin; 
        inputMax = RobotMap.C2R_inputMax;     
        
        outputMin = RobotMap.C2R_outputMin;
        outputMax = RobotMap.C2R_outputMax;
        
        //14 Encode ticks per inch...
        this.input = input;
                
            /* Determine the center of the input range and output range */
            inputCenter = Math.abs(inputMax - inputMin) / 2 + inputMin;
            outputCenter = Math.abs(outputMax - outputMin) / 2 + outputMin;

            /* Scale the input range to the output range */
            scale = (outputMax - outputMin) / (inputMax - inputMin);

            /* Apply the transformation */
            result = (input + -inputCenter) * scale + outputCenter;

            /* Constrain to the output range */
            speed = Math.max(Math.min(result, outputMax), outputMin);

       return speed;
       
    }
    
    public double getEncoderDelta(){
    	delta = (Robot.driveUtil.getRightEncoder() - Robot.driveUtil.getLeftEncoder());
    	return delta;
    }
    
    public boolean leftInWindow1(double distance, boolean action){
    	boolean done;
    	if(action){
    		//Straight
        	if((Math.abs(Robot.driveUtil.getLeftEncoder()) <= (distance + RobotMap.ENCODER_S_Window1)) &&
        	   (Math.abs(Robot.driveUtil.getLeftEncoder()) >= (distance - RobotMap.ENCODER_S_Window1))){
        	    	  done = true;
        	 }else{
        	    	  done = false;
        	 }    	
    	}else{
    		//Pivot
        	if((Math.abs(Robot.driveUtil.getLeftEncoder()) <= (distance + RobotMap.ENCODER_P_Window1)) &&
        	   (Math.abs(Robot.driveUtil.getLeftEncoder()) >= (distance - RobotMap.ENCODER_P_Window1))){
        	    	  done = true;
        	}else{
        	    	  done = false;
        	}    	
    	}
    	return done;
    }
    
    public boolean rightInWindow1(double distance, boolean action){
    	boolean done;
    	if(action){
    		//Straight
        	if((Math.abs(Robot.driveUtil.getRightEncoder()) <= (distance + RobotMap.ENCODER_S_Window1)) &&
            	(Math.abs(Robot.driveUtil.getRightEncoder()) >= (distance - RobotMap.ENCODER_S_Window1))){
      	    	   		done = true;
       	    }else{
      	    	   		done = false;
       	    }    	    		
    	}else{
    		//Pivot
        	if((Math.abs(Robot.driveUtil.getRightEncoder()) <= (distance + RobotMap.ENCODER_P_Window1)) &&
        	    (Math.abs(Robot.driveUtil.getRightEncoder()) >= (distance - RobotMap.ENCODER_P_Window1))){
  	    	    		done = true;
   	    	}else{
  	    	    		done = false;
   	    	}    	
    		
    	}
    	return done;
    }
    
    public boolean leftInWindow2(double distance, boolean action){
    	boolean done;
    	if(action){
    		//Straight
        	if((Math.abs(Robot.driveUtil.getLeftEncoder()) <= (distance + RobotMap.ENCODER_S_Window2)) &&
        	   (Math.abs(Robot.driveUtil.getLeftEncoder()) >= (distance - RobotMap.ENCODER_S_Window2))){
        	   		done = true;
        	}else{
        	   		done = false;
        	}
    	}else{
    		//Pivot
        	if((Math.abs(Robot.driveUtil.getLeftEncoder()) <= (distance + RobotMap.ENCODER_P_Window2)) &&
        	   (Math.abs(Robot.driveUtil.getLeftEncoder()) >= (distance - RobotMap.ENCODER_P_Window2))){
        	   		done = true;
        	}else{
        	   		done = false;
        	}
    		
    	}
    	return done;
    }
    
    public boolean rightInWindow2(double distance, boolean action){
    	boolean done;
    	if(action){
    		//Straight
        	if((Math.abs(Robot.driveUtil.getRightEncoder()) <= (distance + RobotMap.ENCODER_S_Window2)) &&
        	   (Math.abs(Robot.driveUtil.getRightEncoder()) >= (distance - RobotMap.ENCODER_S_Window2))){
        	   		done = true;
        	}else{
        	    	done = false;
        	}    		
    	}else{
    		//Pivot
        	if((Math.abs(Robot.driveUtil.getRightEncoder()) <= (distance + RobotMap.ENCODER_P_Window2)) &&
        	   (Math.abs(Robot.driveUtil.getRightEncoder()) >= (distance - RobotMap.ENCODER_P_Window2))){
        	   		done = true;
        	}else{
        	   		done = false;
        	}
    	}
    	return done;
    }

    public boolean leftIsDone(double distance, boolean action){
    	boolean done;
    	if((Math.abs(Robot.driveUtil.getLeftEncoder()) <= (distance + RobotMap.ENCODER_CONSTANT)) &&
    	   (Math.abs(Robot.driveUtil.getLeftEncoder()) >= (distance - RobotMap.ENCODER_CONSTANT))){
    		done = true;
    	}else if(action){ 
    		//Straight
    		if(Math.abs(Robot.driveUtil.getLeftEncoder()) >= (distance + RobotMap.ENCODER_S_Window2)){
    			done = true;
    		}else{
    			done = false;
    		}
    	}else{
    		//Pivot
    		if(Math.abs(Robot.driveUtil.getLeftEncoder()) >= (distance + RobotMap.ENCODER_P_Window2)){
    			done = true;
    		}else{
    			done = false;
    		}
    	}
    	return done;
    }
    
    public boolean rightIsDone(double distance, boolean action){
    	boolean done;
    	if((Math.abs(Robot.driveUtil.getRightEncoder()) <= (distance + RobotMap.ENCODER_CONSTANT)) &&
    	   (Math.abs(Robot.driveUtil.getRightEncoder()) >= (distance - RobotMap.ENCODER_CONSTANT))){
   	    		done = true;
    	}else if(action){ 
    		//Straight
    		if(Math.abs(Robot.driveUtil.getRightEncoder()) >= (distance + RobotMap.ENCODER_S_Window2)){
   	    			done = true;
    		}else{
   	    			done = false;
    		}
    	}else{
    		//Pivot
    		if(Math.abs(Robot.driveUtil.getRightEncoder()) >= (distance + RobotMap.ENCODER_P_Window2)){
   	    			done = true;
    		}else{
   	    			done = false;
    		}
    	}
    	return done;
    }


	
    public void initDefaultCommand() {
        // Set the default command for a subsystem here.
        //setDefaultCommand(new MySpecialCommand());
    	setDefaultCommand(new DriveTank());
    }
    
    public void updateStatus(){
    	SmartDashboard.putNumber("Left Motor Voltage: ", lDriveMaster.getBusVoltage());
    	SmartDashboard.putNumber("Right Number Voltage: ", rDriveMaster.getBusVoltage());
    	SmartDashboard.putNumber("Left Motor Encoder: ", leftDriveEncoder.getRaw());
    	SmartDashboard.putNumber("Right Motor Encoder: ", rightDriveEncoder.getRaw());
    	SmartDashboard.putNumber("Gyro Angle: ", gyro.getAngle());
    }
}

